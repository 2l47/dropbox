# dropbox

## Simple drop-in PHP thingy to receive messages via Discord.
* Based on https://github.com/z4cH3r/Discord-Webhook-Form

### Notes
* Make sure you install php-curl!
* "No backend form verification, users could add new form inputs. That shouldn't really matter because they will just be posted with the message and nothing malicious could be done." - z4cH3r
* To enable error logging, edit /etc/php/7.4/apache2/php.ini and add this line below the commented-out error_log configuration:
* `error_log = /var/log/php_errors.log`
