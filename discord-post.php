<?php
	function SendForm() {
		require_once ("config.php");
		$json_data = json_encode(
			[
				"embeds" => [
					[
						"type" => "rich",
						"title" => "New message",
						//"description" => $_POST["submitter_message"],

						// Timestamp of embed must be formatted as ISO8601
						"timestamp" => date("c", strtotime("now")),

						// Embed left border color in HEX
						"color" => hexdec("7700FF"), // could try 0xwhatever

						// Additional Fields array
						"fields" => [
							[
								"name" => "Name",
								"value" => $_POST["submitter_name"],
								inline => true
							],
							[
								"name" => "Contact",
								"value" => $_POST["submitter_contact"],
								inline => true
							],
							[
								"name" => "Message",
								"value" => $_POST["submitter_message"],
								inline => false
							]
						]
					]
				]
			],
			JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
		);

		// ch = CurlHandle
		$ch = curl_init( $webhook_url );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
		curl_setopt( $ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt( $ch, CURLOPT_HEADER, 0);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

		$response = curl_exec( $ch );
		curl_close( $ch );
		return $response;
	}

	$response = SendForm();
	if ($response == null) {
		print_r("Message sent!");
	} else {
		print_r("Failed to send message. Please try again later.");
		// Comment this out for production!
		print_r($response);
	}
?>
